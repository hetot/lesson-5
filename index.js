function num_of_vowels(my_str) {
    let vowels_list = ['a', 'e', 'i', 'o', 'u']
    let counter = 0
    for (let i = 0; i < my_str.length; i++) {
        counter = vowels_list.includes(my_str[i]) ? counter + 1 : counter
    }
    return counter
}

function days_left(my_date) {
    let new_year = new Date(2022, 1, 1, 0, 0, 0)

    return Math.floor(Math.abs(new_year - my_date) / 1000 / 86400)
}

function num_of_symbols(my_arr) {
    let array_of_symbols = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'.split('')
    let ans = new Array()
    for (let i = 0; i < my_arr.length(); i++) {
        if (array_of_symbols.includes(my_arr[i])) {
            ans.push(my_arr[i])
        }
    }
    return ans
}

function intersection_of_arrays(arr1, arr2) {
    let set1 = new Set(arr1)
    let set2 = new Set(arr2)

    return Array.from(new Set([...set1].filter(i => set2.has(i))))
}

function sum_of_nums(arr) {
    let sum = 0
    for (let i = 0; i < arr.length(); i++) {
        if (Number.isInteger(arr[i])) {
            sum = arr[i] > 0? sum + arr[i]: sum
        }
    }
    return sum
}

function char_counter(text, my_char) {
    let counter = 0
    for (let i = 0; i < text.length; i++) {
        counter = text[i] === my_char? counter + 1: counter
    }
    return counter
}

// console.log(intersection_of_arrays([1, 2, 2, 4, 5, 6, 3546], [2, 6546, 3, 7]))